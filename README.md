# Docker Sickrage
Dockerfile to set up [SickRage](https://github.com/SickRage/SickRage.git)

[![sickrage](https://github.com/SickRage/SickRage/blob/master/gui/slick/images/sickrage.png?raw=true)](https://github.com/SickRage/SickRage.git)

## Repository
Sources are [here](https://gitlab.com/jpiron/docker-sickrage)

## Run
```bash
docker pull jpiron/sickrage
docker run jpiron/sickrage
```
However, you're more likely to run the container with following:
```bash
docker run -d --restart=always --name sickrage \
-p 8081:8081 \
--user $(id -u ${USER}):$(id -g ${USER}) \
-e TZ=$(cat /etc/timezone) \
-v <YOUR_SICKRAGE_DATA_DIRECTORY>:/sickrage/datadir \
-v <YOUR_DOWNLOAD_DIRECTORY>:/downloads \
-v <YOUR_TV_SHOWS_DIRECTORY>:/tv \
jpiron/sickrage
```

## Build
```
VERSION=v2018.06.15-1 make build
```

#### Parameters
* **user**: Docker run parameter to set the user/group to run the container with;
* **TZ**: the timzeone to run the container with;
* **YOUR_SICKRAGE_DATA_DIRECTORY**: if you want Sickrage data directory to be hold in a local directory. If the directory contains a config.ini file it will be used to configure Sickrage.
* **YOUR_DOWNLOAD_DIRECTORY**: provide sickrage with an access to your download directory. Can be used by post-processing scripts.
* **YOUR_TV_SHOWS_DIRECTORY**: provide sickrage with an access to your tv_shows directory. Can also be used by post-processing scripts.

## Caveats
The Dockerfile creates a sickrage user and makes it the owner of the sickrage installation folder.
When using the **user** parameter, the container is ran with a different user which is not the owner of the sickrage installation folder.
As a result auto-updates can't be performed with the **user** parameter.
To perform updates you will have to pull (or build) the latest image version and run it.

## Versions
* **03.07.2018**: Use a Makefile instead of a shell script.
* **12.03.2017**: Enable building a specific version of Sickrage.
* **29.12.2016**: Fix start.sh permissions.
* **05.11.2016**: Remove the PUID/PGID environment variables in favor of the Docker run --user option.
* **02.10.2016**: Add Ansible role URL.
* **21.09.2016**: First release.

.PHONY: all build publish clean purge docker-login check-docker-env

VERSION ?= master
DOCKER_VERSION := $(VERSION:master=latest)
REGISTRY_IMAGE ?= jpiron/sickrage

all: build publish

build:
	@echo -e '\n### BUILD ###'
	docker build \
		-f Dockerfile \
		--build-arg VERSION=$(VERSION) \
		-t $(REGISTRY_IMAGE):$(DOCKER_VERSION) .

publish: docker-login
	@echo -e '\n### PUBLISH ###'
	docker push $(REGISTRY_IMAGE):$(DOCKER_VERSION)

clean:
	@echo -e '\n### CLEAN ###'
	docker images --filter "reference=$(REGISTRY_IMAGE):$(docker_version)" --quiet 2>/dev/null | xargs --no-run-if-empty docker rmi --force

purge: clean
	@echo -e '\n### PURGE ###'
	docker images --filter "reference=$(REGISTRY_IMAGE)" --quiet 2>/dev/null | xargs --no-run-if-empty docker rmi --force

docker-login: check-docker-env
	@echo -e '\n### DOCKER LOGIN ###'
	@docker login -u '$(DOCKER_REGISTRY_USER)' -p '$(DOCKER_REGISTRY_PASSWORD)' 2> /dev/null

check-docker-env:
ifndef DOCKER_REGISTRY_USER
	$(error DOCKER_REGISTRY_USER is undefined)
endif
ifndef DOCKER_REGISTRY_PASSWORD
	$(error DOCKER_REGISTRY_PASSWORD is undefined)
endif

FROM ubuntu:16.04
LABEL maintainer="jonathanpiron@gmail.com"

# Install Sickrage requirements
RUN apt-get update && apt-get install -y \
        unrar-free\
        git-core\
        openssl\
        libssl-dev\
        python2.7\
    && rm -rf /var/lib/apt/lists/*

# Get Sickrage.
ARG VERSION='master'
RUN git clone --depth 1 --branch "${VERSION}" https://github.com/SickRage/SickRage.git /sickrage

# Create Sickrage group and user.
RUN groupadd --system sickrage\
    && useradd --system -s /bin/false -d /sickrage -g sickrage sickrage

RUN mkdir /downloads /sickrage/datadir /tv\
    && chown -R sickrage:sickrage /sickrage /downloads /tv

VOLUME ["/sickrage/datadir", "/downloads", "/tv"]

EXPOSE 8081

USER sickrage

ENTRYPOINT ["/usr/bin/python2.7", "/sickrage/SickBeard.py", "--datadir=/sickrage/datadir/"]
